const kajians = require("../data/kajian.json");
const { prisma } = require("../db");

const seedKajian = async () => {
  const kajianFormatted = kajians.map(
    ({
      title,
      description,
      ustadz,
      thumbnailUrl,
      videoUrl,
      duration,
      category,
    }) => {
      return {
        title,
        description,
        ustadz,
        thumbnailUrl,
        videoUrl,
        duration,
        category,
      };
    }
  );

  await prisma.kajian.deleteMany();

  await prisma.kajian.createMany({ data: kajianFormatted });
};

seedKajian();
