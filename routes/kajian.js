const router = require("express").Router();
const { prisma } = require("../db");

router.get("/list", async (req, res) => {
  const offset = parseInt(req.query.offset);
  const count = await prisma.kajian.count();
  const kajians = await prisma.kajian.findMany({
    take: 4,
    skip: offset,
  });
  setTimeout(() => {
    return res.json({ kajians, count });
  }, 3000);
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;
  const kajian = await prisma.kajian.findUnique({
    where: {
      id: parseInt(id),
    },
  });
  return res.send(kajian);
});

module.exports = router;
